//
//  HomePageController.swift
//  Actividad5Reproductor
//
//  Created by Carlos Iglesias on 4/5/19.
//  Copyright © 2019 Carlos Iglesias. All rights reserved.
//

import UIKit
import FirebaseAuth

class HomePageController: UIViewController {
    
    var logueado = false
    @IBOutlet weak var loguedText: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        estasLogueado()
        
    }
    
    func estasLogueado(){
       var handle = Auth.auth().addStateDidChangeListener { (auth, user) in
            
            if Auth.auth().currentUser != nil {
                self.loguedText.text = "Si hay usuario logueado"
                self.logueado = true
            } else {
                self.loguedText.text = "Ningun usuario logueado"
                self.logueado = false
                self.performSegue(withIdentifier: "noLogged", sender: nil)
            }
        }

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation*/
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "noLogged"){
            let controller = segue.destination as! LogInController
            
        }else if(segue.identifier == "goToRegister"){
            let controller = segue.destination as! RegisterController
        }
        
    }
    

}
