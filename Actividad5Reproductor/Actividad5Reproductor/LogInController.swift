//
//  LogInController.swift
//  Actividad5Reproductor
//
//  Created by Carlos Iglesias on 3/28/19.
//  Copyright © 2019 Carlos Iglesias. All rights reserved.
//

import UIKit
import FirebaseAuth

class LogInController: UIViewController {
    
    @IBOutlet weak var loged: UILabel!
    var logueado = false
    

    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var emailUser: UITextField!
    @IBOutlet weak var userPassword: UITextField!
    var accessPermission = false
    @IBAction func LogInBtn(_ sender: UIButton) {
        
        Auth.auth().signIn(withEmail: emailUser.text ?? "", password: userPassword.text ?? "") { (user, error) in
            if let firebaseError = error{
                print(firebaseError.localizedDescription)
                let alert = UIAlertController(title: " Error", message: "Usuario o contraseña incorrecto.", preferredStyle: UIAlertController.Style.alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                return
            }else{
                print("Success")
               // accessPermission = true
                self.performSegue(withIdentifier: "logInSegue", sender: nil)
            }
        }
    }
    
    
    @IBAction func goToRecoveryBtn(_ sender: UIButton) {
        self.performSegue(withIdentifier: "goToRecovery", sender: nil)
    }
    
    @IBAction func goToRegisterBtn(_ sender: UIButton) {
        self.performSegue(withIdentifier: "goToRegister", sender: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        background.image = UIImage.init(named: "fondo login")
        self.view.sendSubviewToBack(background)
        self.hideKeyboardWhenTappedAround()
        
         estasLogueado()
        vamonos()
    }
    
    func estasLogueado(){
        if Auth.auth().currentUser != nil {
            loged.text = "Si hay usuario logueado"
            vamonos()
            logueado = true
        } else {
            loged.text = "Ningun usuario logueado"
            logueado = false
        }
    }
    func vamonos(){
        if(logueado == true){
            self.performSegue(withIdentifier: "logInSegue", sender: nil)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "logInSegue"){
            let controller = segue.destination as! HomePageController
           
        }else if(segue.identifier == "goToRegister"){
            let controller = segue.destination as! RegisterController
            
        }else if(segue.identifier == "goToRecovery"){
            let controller = segue.destination as! RestablecerPasswordController
        }
    }
}
