//
//  MusicSingleTon.swift
//  Actividad5Reproductor
//
//  Created by Carlos Iglesias on 2/19/19.
//  Copyright © 2019 Carlos Iglesias. All rights reserved.
//

import Foundation
import UIKit

class MusicSingleTon{

    private static var object = MusicSingleTon()
    var songs = [Song]()
    var index = 0

    
    init(){
        self.fakeSongs()
    }
    
    static func getMusicSingleTon() -> MusicSingleTon{
            return self.object
    }
    
    private func fakeSongs(){
        
        songs.append(Song(url: "sexOnFire", name:"Sex On Fire", artist:"Kings of Lion", image: #imageLiteral(resourceName: "sexOnFire")))
        songs.append(Song(url: "American_Idiot",name:"American Idiot", artist:"Green Day", image:#imageLiteral(resourceName: "american")))
        songs.append(Song(url: "runaway",name:"Runaway", artist:"Bon Jovi", image:#imageLiteral(resourceName: "runaway")))
        songs.append(Song(url:"rockYou",name:"Rock you like a hurricane", artist:"Scorpions", image:#imageLiteral(resourceName: "rockYou")))
        songs.append(Song(url: "sweet",name:"Sweet Child O' Mine",artist:"Guns n Roses", image:#imageLiteral(resourceName: "sweet")))
        songs.append(Song(url:"noOneLikeYou",name:"No one like you", artist:"Scorpions", image:#imageLiteral(resourceName: "noOneLikeYou")))
        songs.append(Song(url:"rapeMe",name:"Rape me", artist:"Nirvana",image: #imageLiteral(resourceName: "rapeMe")))
        songs.append(Song(url:"cantHelpIt",name:"Cant help it", artist:"Anarbor", image: #imageLiteral(resourceName: "cantHelpIt")))
        
    }
    
    
}
