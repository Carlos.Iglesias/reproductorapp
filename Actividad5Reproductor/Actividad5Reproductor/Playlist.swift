//
//  Playlist.swift
//  Actividad5Reproductor
//
//  Created by Carlos Iglesias on 4/2/19.
//  Copyright © 2019 Carlos Iglesias. All rights reserved.
//

import Foundation
import UIKit

class Playlist{
    private var PlaylistUrl: String
    private var PlaylistName: String
    private var PlaylistImage: UIImage
    
    init(PlaylistUrl:String, PlaylistName:String, PlaylistImage: UIImage){
        self.PlaylistUrl = PlaylistUrl
        self.PlaylistName = PlaylistName
        self.PlaylistImage = PlaylistImage
    }
    
    func getPlaylistUrl() -> String{
        return self.PlaylistUrl
    }
    func setPlaylistUrl(PlaylistUrl: String){
        self.PlaylistUrl = PlaylistUrl
    }
    
    func getPlaylistName() -> String{
        return self.PlaylistName
    }
    func setPlaylistName(PlaylistName: String){
        self.PlaylistName = PlaylistName
    }
    
    func getPlaylistImage() -> UIImage{
        return self.PlaylistImage
    }
    func setPlaylistImage(PlaylistImage:UIImage){
        self.PlaylistImage = PlaylistImage
    }
    
}
