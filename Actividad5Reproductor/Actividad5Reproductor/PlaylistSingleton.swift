//
//  PlaylistSingleton.swift
//  Actividad5Reproductor
//
//  Created by Carlos Iglesias on 3/28/19.
//  Copyright © 2019 Carlos Iglesias. All rights reserved.
//

import Foundation
import UIKit

class PlaylistSingleton{
    
    private static var playlist = PlaylistSingleton()
    //var playlists = [Playlist]()
    var index = 0
    
    init(){
        self.fakePlaylist()
    }
    
    static func getPlaylistSingleton() ->
        PlaylistSingleton{
            return self.playlist
    }
    
    private func fakePlaylist(){
        
    }
}
