//
//  Preferencias.swift
//  Actividad5Reproductor
//
//  Created by Carlos Iglesias on 2/21/19.
//  Copyright © 2019 Carlos Iglesias. All rights reserved.
//

import UIKit
import FirebaseFirestore

class Preferencias: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
  
    let db = Firestore.firestore()
   
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
    
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
 
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default,reuseIdentifier: "cell")
        guard (tableView.dequeueReusableCell(withIdentifier: "Cell") as? TableCell) != nil else {
            return UITableViewCell()
        }
        
        //cell.textLabel?.text = data
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: "goToAhorro", sender: nil)
    }

 
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
