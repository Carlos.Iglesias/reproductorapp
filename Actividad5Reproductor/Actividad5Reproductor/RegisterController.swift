//
//  RegisterController.swift
//  Actividad5Reproductor
//
//  Created by Carlos Iglesias on 3/28/19.
//  Copyright © 2019 Carlos Iglesias. All rights reserved.
//

import UIKit
import FirebaseAuth

class RegisterController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var repassword: UITextField!
    
    @IBAction func SignUpBtn(_ sender: UIButton) {
        
        if(userName.text==nil || userName.text==""){
            userName.textColor = UIColor.red
        }else if(lastName.text==nil || lastName.text==""){
            lastName.textColor = UIColor.red
        }else if(email.text==nil || email.text==""){
            email.textColor = UIColor.red
        }else if(password.text==nil || password.text==""){
            password.textColor = UIColor.red
        }else if(repassword.text==nil || repassword.text=="" || repassword.text != password.text){
            repassword.textColor = UIColor.red
        }else{
            
            Auth.auth().createUser(withEmail: email.text ?? "", password: password.text ?? "") { (authResult, error) in
                if let firebaseError = error{
                    print(firebaseError.localizedDescription)
                    return
                }else{
                    print("Success")
                    self.performSegue(withIdentifier: "SignUpSegue", sender: nil)
                }
                guard let user = authResult?.user else { return }
            }
            
            /*
             **Crear objeto usuario y enviarlo**
             **usar merge para no borrar todo alv**
             
             let usuariosRef = db.collection("usuarios")
             alumnosRef.document("usuario").setData([
             "admin@admin.com" : ["nombre" : "Carlos", "apellido" : "Iglesias", "email" : "admin@admin.com", "pass" : "admin"]
             ])
             
             
            let alumnosRef = db.collection("usuarios")
            alumnosRef.document("usuario").setData([
             **enviar objeto aqui**
             
                "nombre": "Carlos",
                "Matricula": "2726784",
                "Apellido": "Iglesias"
                ])*/
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imageView.image = UIImage(named: "fondo login")
        self.view.sendSubviewToBack(imageView)
        
        self.hideKeyboardWhenTappedAround()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let controller = segue.destination as! HomePageController
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
