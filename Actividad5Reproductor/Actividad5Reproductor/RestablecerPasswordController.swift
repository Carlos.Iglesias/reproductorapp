//
//  RestablecerPasswordController.swift
//  Actividad5Reproductor
//
//  Created by Carlos Iglesias on 4/10/19.
//  Copyright © 2019 Carlos Iglesias. All rights reserved.
//

import UIKit
import FirebaseAuth

class RestablecerPasswordController: UIViewController {

    
    @IBOutlet weak var emailRecovery: UITextField!
    @IBAction func RecoverPassword(_ sender: UIButton) {
        Auth.auth().sendPasswordReset(withEmail: emailRecovery.text ?? "") { error in
            if let firebaseError = error{
                print(firebaseError.localizedDescription)
                let alert = UIAlertController(title: " Error", message: "Usuario no encontrado.", preferredStyle: UIAlertController.Style.alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                return
            }else{
                print("Success")
                // create the alert
                let alert = UIAlertController(title: "Listo!", message: "Hemos enviado un email al correo proporcionado.", preferredStyle: UIAlertController.Style.alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { action in
                    
                    // do something like...
                    self.performSegue(withIdentifier: "goToLogin", sender: nil)
                    
                    }))

                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                
        }
    }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let controller = segue.destination as! LogInController
    }
    

}
