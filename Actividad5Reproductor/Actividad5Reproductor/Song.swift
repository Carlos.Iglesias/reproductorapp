//
//  Song.swift
//  Actividad5Reproductor
//
//  Created by Carlos Iglesias on 2/19/19.
//  Copyright © 2019 Carlos Iglesias. All rights reserved.
//

import Foundation
import UIKit

class Song{
    private var url: String
    private var name: String
    private var image: UIImage
    private var artist: String
    
    init(url:String, name:String, artist: String, image: UIImage){
        self.url = url
        self.name = name
        self.artist = artist
        self.image = image
    }
    
    func getUrl() -> String{
        return self.url
    }
    func setUrl(url: String){
        self.url = url
    }
    
    func getName() -> String{
        return self.name
    }
    func setName(name: String){
        self.name = name
    }
    
    func getImage() -> UIImage{
        return self.image
    }
    func setImage(image:UIImage){
        self.image = image
    }
    func getArtist() -> String{
        return self.artist
    }
    func setArtist(artist: String){
        self.artist = artist
    }
    
    
}
