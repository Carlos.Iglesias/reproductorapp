//
//  ViewController.swift
//  Actividad5Reproductor
//
//  Created by Carlos Iglesias on 2/17/19.
//  Copyright © 2019 Carlos Iglesias. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource  {

    @IBOutlet weak var table: UITableView!
    
    var songArray = [Song]()
    var number = 0
    
    var object = MusicSingleTon.getMusicSingleTon()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //setUpSongs()
     
    }
    
   /* private func setUpSongs(){
        songArray.append(Song(name:"Sex On Fire", artist:"Kings of Lion", image:"sexOnFire"))
        songArray.append(Song(name:"American Idiot", artist:"Green Day", image:"american"))
        songArray.append(Song(name:"Runaway", artist:"Bon Jovi", image:"runaway"))
        songArray.append(Song(name:"Rock you like a hurricane", artist:"Scorpions", image:"rockYou"))
        songArray.append(Song(name:"Sweet Child O' Mine",artist:"Guns n Roses", image:"sweet"))
        songArray.append(Song(name:"No one like you", artist:"Scorpions", image:"noOneLikeYou"))
        songArray.append(Song(name:"Rape me", artist:"Nirvana",image:"rapeMe"))
        songArray.append(Song(name:"Cant help it", artist:"Anarbor", image:"cantHelpIt"))
    }*/
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return object.songs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? TableCell else{
            return UITableViewCell()
        }
        cell.nameSong.text = object.songs[indexPath.row].getName()
        cell.nameArtist.text = object.songs[indexPath.row].getArtist()
        cell.imageSong.image = object.songs[indexPath.row].getImage()
       /* cell.nameSong.text = songArray[indexPath.row].name
        cell.nameArtist.text = songArray[indexPath.row].artist
        cell.imageSong.image = UIImage(named:songArray[indexPath.row].image)*/
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        object.index = indexPath.row
        performSegue(withIdentifier: "goToMusic", sender: nil)
    }
    
   /* class Song{
        private let name: String
        private let image: String
        private let artist: String
        
        init(name:String, artist: String, image: String){
            self.name = name
            self.artist = artist
            self.image = image
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let controller = segue.destination as! ViewController2
        controller.passer = number
    }*/

}

