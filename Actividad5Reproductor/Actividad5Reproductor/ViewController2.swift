//
//  ViewController2.swift
//  Actividad5Reproductor
//
//  Created by Carlos Iglesias on 2/17/19.
//  Copyright © 2019 Carlos Iglesias. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController2: UIViewController {

    var passer = 0;
    //var songArray = [Song2]()
    var player = AVAudioPlayer()
    var ticTac = Timer()
    var object = MusicSingleTon.getMusicSingleTon()
    
    @IBOutlet weak var albumImage: UIImageView!
    @IBOutlet weak var nombreCancion: UILabel!
    @IBOutlet weak var nombreArtista: UILabel!
    
    @IBAction func goHome(_ sender: UIButton) {
        player.stop()
    }
    
    @IBOutlet weak var timeSlider: UISlider!
    @IBAction func time(_ sender: UISlider) {
        player.stop()
        player.currentTime = TimeInterval(timeSlider.value)
        player.prepareToPlay()
        player.play()
    }
    
    @IBOutlet weak var back: UIButton!
    @IBAction func backBtn(_ sender: UIButton) {
        player.stop()
        player.currentTime = 0
        player.play()
    }
    
    
    @IBOutlet weak var play: UIButton!
    @IBAction func playBtn(_ sender: UIButton) {
        if(player.isPlaying){
            player.pause()
            ticTac.invalidate()
            play.setImage(UIImage(named: "play3"), for: .normal)
        }else{
            player.play()
            play.setImage(UIImage(named: "pause3"), for: .normal)
            
        }
    }
    
    
    @IBOutlet weak var nxt: UIButton!
    @IBAction func nextBtn(_ sender: UIButton) {
        player.stop()
        if(object.index < object.songs.count){
            object.index = object.index+1
        }else{
         object.index = 0
        }

        startPlaying()
    }
    
    
    @IBOutlet weak var volume: UISlider!
    @IBAction func volumeSlider(_ sender: UISlider) {
        player.volume = volume.value
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
            //setUpSongs()
            startPlaying()
            startTimer()
    }
   /* private func setUpSongs(){
        songArray.append(Song2(name:"Sex On Fire", artist:"Kings of Lion", image:"sexOnFire",song:"sexOnFire"))
        songArray.append(Song2(name:"American Idiot", artist:"Green Day", image:"american",song:"American_Idiot"))
        songArray.append(Song2(name:"Runaway", artist:"Bon Jovi", image:"runaway", song:"runaway"))
        songArray.append(Song2(name:"Rock you like a hurricane", artist:"Scorpions", image:"rockYou",song:"rockYou"))
        songArray.append(Song2(name:"Sweet Child O' Mine",artist:"Guns n Roses", image:"sweet",song:"sweet"))
        songArray.append(Song2(name:"No one like you", artist:"Scorpions", image:"noOneLikeYou",song:"noOneLikeYou"))
        songArray.append(Song2(name:"Rape me", artist:"Nirvana",image:"rapeMe",song:"rapeMe"))
        songArray.append(Song2(name:"Cant help it", artist:"Anarbor", image:"cantHelpIt",song:"cantHelpIt"))
    }

    class Song2{
        let name: String
        let image: String
        let artist: String
        let song: String
        
        init(name:String, artist: String, image: String,song:String){
            self.name = name
            self.artist = artist
            self.image = image
            self.song = song
        }
    }*/
    
    private func startPlaying(){
        do{
            try player = AVAudioPlayer(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: object.songs[object.index].getUrl() , ofType:"mp3")!))
            
        }catch{
            
        }
        //albumImage.image = UIImageView(object.songs[object.index].getImage())
        albumImage.image = object.songs[object.index].getImage()
        nombreCancion.text = object.songs[object.index].getName()
        nombreArtista.text = object.songs[object.index].getArtist()
        player.play()
    }
    
    private func startTimer(){
        ticTac = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateSlider), userInfo: nil, repeats: true)
             timeSlider.maximumValue = Float(player.duration)
    }

    @objc func updateSlider(){
        timeSlider.value = Float(player.currentTime)
    }
    
}
