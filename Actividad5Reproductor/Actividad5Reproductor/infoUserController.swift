//
//  infoUserController.swift
//  Actividad5Reproductor
//
//  Created by Carlos Iglesias on 4/23/19.
//  Copyright © 2019 Carlos Iglesias. All rights reserved.
//

import UIKit
import FirebaseAuth

class infoUserController: UIViewController {

    
    @IBAction func backBtn(_ sender: UIButton) {
        self.performSegue(withIdentifier: "goToLibrary", sender: nil)
    }
    
    
    @IBAction func logoutBtn(_ sender: UIButton) {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            self.performSegue(withIdentifier: "logoutSegue", sender: nil)
        
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
            let alert = UIAlertController(title: " Error", message: "Error al hacer logout.", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        identifyUser()
    }
    
    func identifyUser(){
        let user = Auth.auth().currentUser
        if let user = user {

            let email = user.email
            let uid = user.uid
            
            emailLabel.text = email
            passwordLabel.text = uid
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation*/
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "goToLibrary"){
            let controller = segue.destination as! Library
        }else{
            if(segue.identifier == "LogoutSegue"){
                let controller = segue.destination as! LogInController
            }
        }
    }

}
